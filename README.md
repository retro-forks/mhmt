# mhmt

CLI packers/depackers for formats used on ZX Spectrum 8-bit. Also contains Z80 8-bit depackers.

Aggregates in single prog optimal packers for megalz format and hrum format, nearly-optimal packer for hrust.

~~~~
======== mhmt help ========
parameters:
-mlz, -hrm, -hst - use MegaLZ, hrum3.5 or hrust1.x format (default is MegaLZ)
-g - greedy coding (default is optimal coding)
-d - depacking instead of packing (default is packing)

-zxh - use zx-specific header for hrum or hrust. DEFAULT is NO HEADER!
       Not applicable for MegaLZ. If -zxh is specified, -16, NO -bend and
       NO -mlz is forced.

-8, -16 - bitstream is in bytes or words in packed file.
          Default for MegaLZ is -8, for hrum and hrust is -16.

-bend - if -16 specified, this makes words big-endian. Default is little-endian.

-maxwinN - maximum lookback window. N is decimal number, which can only be
           256,512,1024,2048,4096,8192,16384,32768. Default is format-specific
           maximum window: MegaLZ is 4352, hrum is 4096, hrust is 65536.
           For given format, window can't be greater than default value

-prebin <filename> - use specified file as prebinary for packing and depacking.

usage:
mhmt [parameter list] <input filename> [<output filename>]

if no output filename given, filename is appended with ".mlz", ".hrm" or ".hst"
in accordance with format chosen; for depacking ".dpk" is appended
====== mhmt help end ======
~~~~
